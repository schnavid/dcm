(ns ui.name-line)

(defn name-line [state]
  [:div.name-line
   [:span.label "Name"]
   [:input.name {:on-change #(swap! state assoc :name (-> % .-target .-value))
                 :value (:name @state)
                 :type "text"}]])