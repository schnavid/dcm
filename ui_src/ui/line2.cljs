(ns ui.line2)

(defn line2 [state]
  [:div.line
   [:div#labels2.labels
    [:span.label "Gesinnung"]
    [:span.label "Rasse"]
    [:span.label "Hintergrund"]]
   [:div#line2.inputs
    [:select#alignment {:on-change #(swap! state assoc :alignment (-> % .-target .-value))
                        :value (:alignment @state)}
     [:option {:value "lg"} "Rechtschaffen Gut"]
     [:option {:value "ng"} "Neutral Gut"]
     [:option {:value "cg"} "Chaotisch Gut"]
     [:option {:value "ln"} "Rechtschaffen Neutral"]
     [:option {:value "nn"} "Neutral"]
     [:option {:value "cn"} "Chaotisch Neutral"]
     [:option {:value "le"} "Rechtschaffen Böse"]
     [:option {:value "ne"} "Neutral Böse"]
     [:option {:value "ce"} "Chaotisch Böse"]]
    [:input#race {:on-change #(swap! state assoc :race (-> % .-target .-value))
                  :value (:race @state)
                  :type "text"}]
    [:input#background {:on-change #(swap! state assoc :background (-> % .-target .-value))
                        :value (:background @state)
                        :type "text"}]]])