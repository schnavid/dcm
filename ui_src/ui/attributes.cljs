(ns ui.attributes)

(defn val-to-mod-num [val]
  (Math/floor (/ (- val 10) 2)))

(defn val-to-mod [val]
  (let [val-int (js/parseInt val)
        mod (val-to-mod-num val-int)]
    (if (> mod 0)
      (str "+" mod)
      (str mod))))

(defn strength [state]
  [:div#strength.attribute
   [:span.attribute-label.label "STR"]
   [:span.attribute-mod
    (val-to-mod (:strength @state))]
   [:input.attribute-value {:on-change #(swap! state assoc :strength (-> % .-target .-value))
                            :value (:strength @state)
                            :type "number"}]])

(defn dexterity [state]
  [:div#dexterity.attribute
   [:span.attribute-label.label "GES"]
   [:span.attribute-mod
    (val-to-mod (:dexterity @state))]
   [:input.attribute-value {:on-change #(swap! state assoc :dexterity (-> % .-target .-value))
                            :value (:dexterity @state)
                            :type "number"}]])

(defn constitution [state]
  [:div#constitution.attribute
   [:span.attribute-label.label "KON"]
   [:span.attribute-mod
    (val-to-mod (:constitution @state))]
   [:input.attribute-value {:on-change #(swap! state assoc :constitution (-> % .-target .-value))
                            :value (:constitution @state)
                            :type "number"}]])

(defn intelligence [state]
  [:div#intelligence.attribute
   [:span.attribute-label.label "INT"]
   [:span.attribute-mod
    (val-to-mod (:intelligence @state))]
   [:input.attribute-value.right {:on-change #(swap! state assoc :intelligence (-> % .-target .-value))
                                  :value (:intelligence @state)
                                  :type "number"}]])

(defn wisdom [state]
  [:div#wisdom.attribute
   [:span.attribute-label.label "WEI"]
   [:span.attribute-mod
    (val-to-mod (:wisdom @state))]
   [:input.attribute-value.right {:on-change #(swap! state assoc :wisdom (-> % .-target .-value))
                                  :value (:wisdom @state)
                                  :type "number"}]])

(defn charisma [state]
  [:div#charisma.attribute
   [:span.attribute-label.label "CHA"]
   [:span.attribute-mod
    (val-to-mod (:charisma @state))]
   [:input.attribute-value.right {:on-change #(swap! state assoc :charisma (-> % .-target .-value))
                                  :value (:charisma @state)
                                  :type "number"}]])