(ns ui.core
  (:require [reagent.core :as reagent :refer [atom]]
            [cljs-node-io.core :as io :refer [slurp spit]]
            [cljs.reader :refer [read-string]]
            [ui.attributes :refer [val-to-mod-num strength dexterity constitution intelligence wisdom charisma]]
            [ui.xp :refer [xp-label nextxp-label xp-inputs level-circle level-label]]
            [ui.line2 :refer [line2]]
            [ui.line3 :refer [line3]]
            [ui.name-line :refer [name-line]]))

(enable-console-print!)

(defn load-char-file []
  (try (read-string (slurp "data.edn"))
       (catch :default _ {:name "test"})))

(defonce character (atom (load-char-file)))

(defn save-char-file []
  (spit "data.edn" @character))

(defn hp [state]
  [:div#hp
   [:span#hp-label.label "Trefferpunkte"]
   [:div#hp-line
    [:input.hp-input {:on-change #(swap! state assoc :hp (-> % .-target .-value))
                      :value (:hp @state)
                      :type "number"}]
    [:span#hp-separator "/"]
    [:input.hp-input {:on-change #(swap! state assoc :maxhp (-> % .-target .-value))
                      :value (:maxhp @state)
                      :type "number"}]]])

(defn ac [state]
  [:div#ac
   [:span#ac-label.label "Rüstungsklasse"]
   [:div#ac-line
    [:input.ac-input {:on-change #(swap! state assoc :ac (-> % .-target .-value))
                      :value (:ac @state)
                      :type "number"}]]])

(defn get-proficiency-bonus [state]
  (cond
    (>= (ui.xp/get-level state) 17) 6
    (>= (ui.xp/get-level state) 13) 5
    (>= (ui.xp/get-level state) 9) 4
    (>= (ui.xp/get-level state) 5) 3
    (>= (ui.xp/get-level state) 1) 2))

(defn proficiency-bonus [state]
  [:div#proficiency-bonus
   [:span#proficiency-bonus-label.label "Übungsbonus"]
   [:div#proficiency-bonus-line
    [:span.proficiency-bonus-span (str "+" (get-proficiency-bonus state))]]])

(defn get-perception [state]
  (+ 10
     (val-to-mod-num (:wisdom @state))
     (if (contains? (:skills @state) :perception)
       (get-proficiency-bonus state)
       0)))

(defn perception [state]
  [:div#perception
   [:span#perception-label.label "Wahrnehmung"]
   [:div#perception-line
    [:span.perception-span (get-perception state)]]])

(defn speed [state]
  [:div#speed
   [:span#speed-label.label "Geschwindigkeit"]
   [:div#speed-line
    [:input.speed-input {:on-change #(swap! state assoc :speed (-> % .-target .-value))
                              :value (:speed @state)
                              :type "text"}]]])
(defn initiative [state]
  [:div#initiative
   [:span#initiative-label.label "Initiative"]
   [:div#initiative-line
    [:input.initiative-input {:on-change #(swap! state assoc :initiative (-> % .-target .-value))
                              :value (:initiative @state)
                              :type "number"}]]])

(defn main-info [state]
  [:div.main-info
   (name-line state)
   (line2 state)
   (line3 state)
   (level-label)
   (xp-inputs state)
   (level-circle state)
   (xp-label)
   (nextxp-label)
   [:span#inspiration-label.label "Inspiration"]
   (strength state)
   (dexterity state)
   (constitution state)
   (intelligence state)
   (wisdom state)
   (charisma state)
   (hp state)
   (ac state)
   (proficiency-bonus state)
   (initiative state)
   (speed state)
   (perception state)])

(defn root-component []
  [:div
   [:button.save {:on-click #(save-char-file)}
    "Save"]
   [:div.sheet
    [:div#outer-circle
     (main-info character)
     [:div#inspiration-circle
      [:input#inspiration {:on-change #(swap! character assoc :inspiration (-> % .-target .-checked))
                           :value "given"
                           :checked (:inspiration @character)
                           :type "checkbox"}]]]]])

(reagent/render
 [root-component]
 (js/document.getElementById "app-container"))
