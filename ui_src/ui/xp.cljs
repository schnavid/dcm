(ns ui.xp)

(defn get-level [state]
  (cond
    (>= (:xp @state) 355000) 20
    (>= (:xp @state) 305000) 19
    (>= (:xp @state) 265000) 18
    (>= (:xp @state) 225000) 17
    (>= (:xp @state) 195000) 16
    (>= (:xp @state) 165000) 15
    (>= (:xp @state) 140000) 14
    (>= (:xp @state) 120000) 13
    (>= (:xp @state) 100000) 12
    (>= (:xp @state) 85000) 11
    (>= (:xp @state) 64000) 10
    (>= (:xp @state) 48000) 9
    (>= (:xp @state) 34000) 8
    (>= (:xp @state) 23000) 7
    (>= (:xp @state) 14000) 6
    (>= (:xp @state) 6500) 5
    (>= (:xp @state) 2700) 4
    (>= (:xp @state) 900) 3
    (>= (:xp @state) 300) 2
    (>= (:xp @state) 0) 1))

(defn get-next-xp [state]
  (cond
    (>= (:xp @state) 355000) "-"
    (>= (:xp @state) 305000) 355000
    (>= (:xp @state) 265000) 305000
    (>= (:xp @state) 225000) 265000
    (>= (:xp @state) 195000) 225000
    (>= (:xp @state) 165000) 195000
    (>= (:xp @state) 140000) 165000
    (>= (:xp @state) 120000) 140000
    (>= (:xp @state) 100000) 120000
    (>= (:xp @state) 85000) 100000
    (>= (:xp @state) 64000) 85000
    (>= (:xp @state) 48000) 64000
    (>= (:xp @state) 34000) 48000
    (>= (:xp @state) 23000) 34000
    (>= (:xp @state) 14000) 23000
    (>= (:xp @state) 6500) 14000
    (>= (:xp @state) 2700) 6500
    (>= (:xp @state) 900) 2700
    (>= (:xp @state) 300) 900
    (>= (:xp @state) 0) 300
    (< (:xp @state) 0) 0))

(defn xp-label [] [:span#xp.label "Erfahrung"])
(defn nextxp-label [] [:span#nextxp.label "Nächstes Level"])
(defn level-label [] [:span.level.label "Level"])

(defn level-circle [state]
  [:div#levelcircle
   [:span.level (get-level state)]])

(defn xp-inputs [state]
  [:div#line4.inputs
   [:input#classes {:on-change #(swap! state assoc :xp (-> % .-target .-value))
                    :value (:xp @state)
                    :type "number"}]
   [:span#classes (get-next-xp state)]])