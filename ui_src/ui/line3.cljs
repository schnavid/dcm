(ns ui.line3)

(defn line3 [state]
  [:div.line
   [:div.labels
    [:span.label "Klasse(n)"]]
   [:div#line3.inputs
    [:input#classes {:on-change #(swap! state assoc :classes (-> % .-target .-value))
                     :value (:classes @state)
                     :type "text"}]]])